import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexBoardComponent } from './profissional/index-board/index-board.component';
import { CadastroPessoalComponent } from './profissional/index-board/cadastro-pessoal/cadastro-pessoal.component';
import { CadastroPessoalDashComponent } from './profissional/index-board/cadastro-pessoal/dash/cadastro-pessoal-dash.component';
import { CadastroPessoalFormComponent } from './profissional/index-board/cadastro-pessoal/form/cadastro-pessoal-form.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexBoardComponent,
    CadastroPessoalComponent,
    CadastroPessoalDashComponent,
    CadastroPessoalFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
