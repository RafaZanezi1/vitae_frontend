import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroPessoalDashComponent } from './cadastro-pessoal-dash.component';

describe('CadastroPessoalDashComponent', () => {
  let component: CadastroPessoalDashComponent;
  let fixture: ComponentFixture<CadastroPessoalDashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroPessoalDashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroPessoalDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
