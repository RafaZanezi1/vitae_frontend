import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroPessoalFormComponent } from './cadastro-pessoal-form.component';

describe('CadastroPessoalFormComponent', () => {
  let component: CadastroPessoalFormComponent;
  let fixture: ComponentFixture<CadastroPessoalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroPessoalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroPessoalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
