import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index-board',
  templateUrl: './index-board.component.html',
  styleUrls: ['./index-board.component.scss']
})
export class IndexBoardComponent implements OnInit {

  bExibeDetalhe: boolean;
  constructor() {
    this.bExibeDetalhe = true; // valor inicial deve ser true
  }

  ngOnInit() { }

  showDash(nDashIndex: number) {
    this.bExibeDetalhe = !this.bExibeDetalhe;
    console.log(nDashIndex);
  }

}
